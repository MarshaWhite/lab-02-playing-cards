#include <iostream>

/*
Write a C++ program that utilizes enumerations to create playing cards.
  Part 1:
    Create a program that has a structure for playing cards.
    Call the struct "Card."
    The rank and suit should be enumerations.
    Note: an Ace should be a high card with a value of 14.
*/

// Assuming we're not using headers here, either.

template <typename... Args>
auto print(std::ostream& os, const Args& ...args) -> std::ostream&
{
    return (os << ... << args);
}

template <typename... Args>
auto print(const Args& ...args) -> std::ostream&
{
    return print(std::cout, args...);
}

template <typename... Args>
auto print_bool(const Args& ...args) -> std::ostream&
{
    std::cout.setf(std::ios::boolalpha);
    return print(args...);
}

/** @brief  Enumeration class Rank holds the value of a playing card.
    @remark "enum class" requires you to use the fully-qualified name,
    preventing its members from polluting the namespace.
    e.g. Rank::Two instead of simply Two. */
enum class Rank {
    Two = 2,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
};

/** @brief  Enumeration class Suit holds the suit of a playing card.
    @remark "enum class" requires you to use the fully-qualified name,
    preventing its members from polluting the namespace.
    e.g.Rank::Two instead of simply Two. */
enum class Suit {
    Clubs,
    Diamonds,
    Hearts,
    Spades,
};

/** @brief  Card struct with a default constructor initializing
            rank to Two and suit to Clubs. */
struct Card {
public: // Explicitly mark these as public for funsies.
    Card() : rank(Rank::Two), suit(Suit::Clubs) { }
    Rank rank;
    Suit suit;
};

int main(int, char**)
{
    // Ensure Ace is a high card with a value of 14.
    // static_assert(static_cast<int>(Rank::Ace) == 14);
    // print_bool(static_cast<int>(Rank::Ace) == 14, '\n');

    return 0;
}